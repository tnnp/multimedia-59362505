import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HomePage} from '../home/home';
import {AboutPage} from '../about/about';
import {ContactPage} from '../contact/contact';

/**
 * Generated class for the AssignmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-assignment',
  templateUrl: 'assignment.html',
})
export class AssignmentPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  homepage()
  {
    this.navCtrl.push(HomePage);
  }
  aboutpage()
  {
    this.navCtrl.push(AboutPage);
  }

  contactpage()
  {
    this.navCtrl.push(ContactPage);
  }
}
