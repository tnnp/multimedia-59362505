import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import {WhalePage} from '../whale/whale';
import {ContactPage} from '../contact/contact';
import {AssignmentPage} from '../assignment/assignment';
/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');

  }

  second()
  {
    this.navCtrl.push(WhalePage);
  }
  API()
  {
    this.navCtrl.push(ContactPage);
  }

  assign()
  {
    this.navCtrl.push(AssignmentPage);
  }
}
