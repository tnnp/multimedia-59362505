import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WhalePage } from './whale';

@NgModule({
  declarations: [
    WhalePage,
  ],
  imports: [
    IonicPageModule.forChild(WhalePage),
  ],
})
export class WhalePageModule {}
