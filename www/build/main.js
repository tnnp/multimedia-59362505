webpackJsonp([3],{

/***/ 117:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AssignmentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__about_about__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__contact_contact__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the AssignmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AssignmentPage = /** @class */ (function () {
    function AssignmentPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AssignmentPage.prototype.homepage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    AssignmentPage.prototype.aboutpage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__about_about__["a" /* AboutPage */]);
    };
    AssignmentPage.prototype.contactpage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__contact_contact__["a" /* ContactPage */]);
    };
    AssignmentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-assignment',template:/*ion-inline-start:"E:\2561-2\Multimedia\Multimedia-59362505\Juju\multimedia-59362505\src\pages\assignment\assignment.html"*/'<!--\n  Generated template for the AssignmentPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="whaleinthai">\n    <ion-title>Assignment</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding class="bg-image font">\n\n    <br/><br/><br/><br/><br/><br/><br/><br/>\n    \n\n    <div class="posiall">\n        <button ion-button (click)="homepage()" color="light" default="true" outline="true" class="center jello" round>\n        <ion-icon color="light" ></ion-icon>\n            Home\n          </button>\n        </div>\n\n        <br/>\n   \n      <div class="posiall">\n          <button ion-button (click)="aboutpage()" color="light" default="true" outline="true" class="center jello" round>\n          <ion-icon color="light" ></ion-icon>\n              About\n            </button>\n          </div>\n          \n          <br/>\n\n      <div class="posiall">\n      <button ion-button (click)="contactpage()" color="light" default="true" outline="true" class="center jello" round>\n      <ion-icon color="light" ></ion-icon>\n          Contact\n        </button>\n      </div>\n      \n      \n</ion-content>\n'/*ion-inline-end:"E:\2561-2\Multimedia\Multimedia-59362505\Juju\multimedia-59362505\src\pages\assignment\assignment.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], AssignmentPage);
    return AssignmentPage;
}());

//# sourceMappingURL=assignment.js.map

/***/ }),

/***/ 118:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__whale_whale__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__contact_contact__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__assignment_assignment__ = __webpack_require__(117);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MenuPage = /** @class */ (function () {
    function MenuPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    MenuPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MenuPage');
    };
    MenuPage.prototype.second = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__whale_whale__["a" /* WhalePage */]);
    };
    MenuPage.prototype.API = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__contact_contact__["a" /* ContactPage */]);
    };
    MenuPage.prototype.assign = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__assignment_assignment__["a" /* AssignmentPage */]);
    };
    MenuPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-menu',template:/*ion-inline-start:"E:\2561-2\Multimedia\Multimedia-59362505\Juju\multimedia-59362505\src\pages\menu\menu.html"*/'<!--\n  Generated template for the MenuPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n<ion-content padding class="bg-image font">\n\n\n<h2 class="topic"> W H A L E </h2>\n<img class="topic1" src= "./assets/img/whalee2.svg" width="300px" height="250" >\n<!-- <button ion-button ="full"(click)="second()">Whale</button>\n<br/>\n  <button ion-button ="full">Assignment</button>\n<br/>   -->\n  \n<br/><br/><br/><br/><br/><br/><br/>\n\n  <button ion-button  (click)="second()" color="see" class="center jello" round>\n    <ion-icon color="light" ></ion-icon>\n    Whale In Thailand\n  </button>\n\n\n\n<div class="posiall">\n<button ion-button  (click)=" API()" color="see" class="center jello" round>\n        <ion-icon color="light" ></ion-icon>\n        API Google\n      </button>\n</div>\n      \n<div class="posiall">\n\n  <button ion-button  (click)="assign()" color="see" class="center jello" round>\n      <ion-icon color="light" ></ion-icon>\n      Assignment\n    </button>\n  </div>\n  \n\n\n</ion-content>\n'/*ion-inline-end:"E:\2561-2\Multimedia\Multimedia-59362505\Juju\multimedia-59362505\src\pages\menu\menu.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], MenuPage);
    return MenuPage;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 119:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WhalePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the WhalePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var WhalePage = /** @class */ (function () {
    function WhalePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.slider = [
            {
                title: 'วาฬในไทย',
                description: 'วาฬชนิดที่เคยมีรายงานการพบหรือเห็นซากในประเทศไทยนับแต่อดีตถึงปัจจุบัน มีทั้งวาฬแบบกรองกิน (วาฬบาลีน – Baleen Whale และวาฬชนิดมีฟัน - Toothed Whale) พบได้ทั้งในอ่าวไทยและอันดามัน มีทั้งขนาดเล็ก (บางชนิดเล็กกว่าโลมา) ไปจนถึงใหญ่มากอย่างวาฬสีน้ำเงินเล็ก (Pygmy Blue Whale)',
                image: "assets/img/0.jpg"
            },
            {
                title: 'Pygmy Blue Whale',
                title2: 'วาฬสีน้ำเงินเล็ก',
                description: 'วาฬสีน้ำเงินเล็กชนิดที่พบได้แถบทะเลเขตร้อนเป็นชนิดย่อยของวาฬสีน้ำเงิน (Blue Whale) ที่ได้ชื่อว่าเป็นสัตว์ใหญ่ที่สุดในโลก (30-34 เมตร) นอกจากความต่างที่ขนาดจะเล็กกว่าวาฬสีน้ำเงินปกติแล้ว สีผิวหนังจะมีความเข้ม (เทา-ฟ้าเข้ม) มากกว่า หางสั้นกว่า ส่วนหัวขนาดใหญ่และลำตัวเพรียวกว่ามาก (รวมๆ แล้วดูคล้ายทรงลูกอ๊อด ลูกกบ) จึงอาจสับสนกับวาฬฟินหรือวาฬไซ ในประเทศไทยพบทางฝั่งอันดามัน ได้ชื่อว่าเป็นวาฬรักสงบ',
                image: "assets/img/1.jpg"
            },
            {
                title: 'Fin Whale',
                title2: 'วาฬฟิน',
                description: 'เป็นวาฬที่มีขนาดใหญ่รองลงมาจากวาฬสีน้ำเงิน จึงถือได้ว่าเป็นสัตว์ที่ใหญ่เป็นอันดับสองของโลก แต่รูปร่างผอมเพรียวและมีความว่องไวปราดเปรียว (มีผู้เปรียบเทียบว่าเหมือนสุนัข Greyhound) ลักษณะเด่นพิเศษคือสีของกรรไกรล่างไม่เหมือนกัน ด้านซ้ายมีสีเข้มเกือบดำ ส่วนด้านขวาสีจางกว่าเกือบเป็นสีเทา',
                image: "assets/img/2.jpg"
            },
            {
                title: 'Humback Whale ',
                title2: 'วาฬหลังค่อม',
                description: 'ครีบข้างที่ยาวมากเป็นเอกลักษณ์สำคัญ เป็นวาฬที่อยู่ได้ทั่วทุกน่านน้ำ และมีการย้ายถิ่นโดยหากินในเขตหนาว ออกลูกในเขตร้อน ได้ชื่อว่าเป็น “วาฬนักร้อง” และมีระบบการสื่อสารด้วยเสียงที่มีเอกลักษณ์และซับซ้อนที่สุดซึ่งยังเป็นความลึกลับในโลกของนักวิจัยวาฬอยู่จนทุกวันนี้ เป็นวาฬขนาดใหญ่ที่มีพลังและกิจกรรมล้นเหลือ มีทั้งการกระโดด ยกหาง ตีหาง ถือเป็นขวัญใจของนักดูวาฬทั่วโลก',
                image: "assets/img/3.jpg"
            },
            {
                title: 'Bryde’s Whale',
                title2: 'วาฬบรูด้า',
                description: 'จุดเด่นชัดเจนบ่งบอกสายพันธุ์และสังเกตได้ง่ายคือมีแนวสามสันบนหัว ตำแหน่งครีบหลังค่อนไปทางท้ายลำตัว และครีบข้างเล็ก มักอาศัยประจำถิ่นและอยู่บริเวณชายฝั่ง เป็นวาฬชนิดที่มีรายงานการพบบ่อยที่สุดในปัจจุบันโดยเฉพาะบริเวณอ่าวไทย จนได้รับการขนานนามเป็นสัตว์ประจำถิ่นของอ่าวไทย ไม่ชอบรวมฝูง แต่อาจมีบางครั้งที่พบมากถึง 10-20 ตัว ชื่อวาฬชนิดนี้ออกเสียงแตกต่างกันไปตามท้องถิ่น ต่างประเทศมักออกเสียงว่า <บรู-ดุส หรือ บรูดส์>',
                image: "assets/img/4.jpg"
            },
            {
                title: 'Omura Whale',
                title2: 'วาฬโอมูระ',
                description: 'เหมือนบรูด้าราวกับพี่น้อง สามารถแยกแยะจากบรูด้าได้ง่ายด้วยการดูสันบนหัวที่โอมูระมีเพียงเส้นเดียว (บรูด้ามี 3 สัน) และยังมีลักษณะคล้ายวาฬฟินราวกับคู่แฝด แต่มีขนาดเล็กกว่ามาก บางคนจึงเรียกว่าวาฬฟินแคระ (ซึ่งที่จริงแล้วไม่ใช่ เป็นคนละชนิด) และมีความความแตกต่างจากฟินที่ชัดเจนคือสีที่ขากรรไกรล่างแตกต่างกัน และจำนวนร่องใต้คางที่โอมูระมีมากกว่า เป็นวาฬที่พบเฉพาะในเขตร้อน',
                image: "assets/img/5.jpg"
            },
            {
                title: 'Dwarf Minke Whale',
                title2: 'วาฬมิงกี้แคระ',
                description: 'เป็นหนึ่งในชนิดย่อยของวาฬบาลีนขนาดเล็กสุด (Minke แบ่งย่อยออกเป็น 3 ประเภทตามขนาดและแหล่งที่อยู่ ได้แก่ Northern, Dwarf และ Arctic แต่ละชนิดมีลายลำตัวและลายครีบข้างที่ต่างกันด้วย) รูปร่างยาวเพรียวแบบเดียวกับฟินและบรูด้า แต่มีจุดสังเกตเด่นที่ลายครีบข้างเป็นแถบสีขาวขนาดใหญ่คาดกลางครีบอย่างชัดเจน (แต่มิงกี้ในมหาสมุทรแปซิฟิคแถบสีนี้จะออกเทาจางๆ ไม่เป็นสีขาวชัดเจนแบบมิงกี้ในมหาสมุทรแอตแลนติก) แม้ได้ชื่อว่าเป็นวาฬขนาดเล็กสุดแต่ก็ไม่เสมอไป บางทีก็ใหญ่เท่าๆ กับวาฬบรูด้าได้เหมือนกัน',
                image: "assets/img/6.jpg"
            },
            {
                title: 'Sperm Whale',
                title2: 'วาฬหัวทุย',
                description: 'เป็นวาฬชนิดมีฟันที่มีขนาดใหญ่ที่สุด มีเอกลักษณ์ที่หัวสี่เหลี่ยมและรูหายใจเยื้องไปทางด้านซ้ายของหัว ส่วนของหัวที่มีขนาดใหญ่พิเศษบรรจุไขมันจำนวนมหาศาล ฟันมีเฉพาะที่ขากรรไกรล่าง ผิวหนังหนาสีน้ำตาล-เทาและมีรอยยับย่นขรุขระ สามารถพ่นของเหลวคล้ายหมึกใส่ศัตรูได้เพื่อหลบหนี และที่จริงแล้วเสียงของวาฬหัวทุยมีระดับความดังกว่าเสียงวาฬสีน้ำเงิน แต่เวลาที่ส่งเสียงออกมาสั้นมาก ดำน้ำได้ลึกมากถึงระดับ 2,000 ม. เป็นวาฬที่เป็นต้นแบบของเรือดำน้ำรุ่นแรกๆ ของโลก',
                image: "assets/img/7.jpg"
            },
            {
                title: 'Killer Whale',
                title2: 'วาฬเพชฌฆาต',
                description: 'วาฬที่คนทุกเพศทุกวัยรู้จักกันดีในชื่อ “ออร์กา” มักถูกเรียกขานว่า “หมาป่าแห่งท้องทะเล” ด้วยพฤติกรรมการรวมกลุ่มและล่าเป็นฝูง แต่ถึงแม้จะมีชื่อเช่นนี้แต่วาฬเพชฌฆาตในธรรมชาติไม่มีประวัติทำร้ายคน มีเอกลักษณ์ที่สีขาวดำตัดกันชัดเจนของลำตัวและครีบหลังตั้งตรงสูงตระหง่าน มีถิ่นอาศัยแพร่กระจายทั่วโลก',
                image: "assets/img/8.jpg"
            },
            {
                title: 'Melon-headed Whale',
                title2: 'วาฬหัวแตงโม ',
                description: 'คนมักเข้าใจผิดกับวาฬเพชฌฆาตเล็ก (Pygmy Killer Whale) ให้สังเกตที่หัวซึ่งแม้จะกลมมนเหมือนกัน แต่มีความลาดมากกว่าและปากจะแหลมกว่า มีวงดำรอบริมฝีปาก สีผิวเทาเข้มกว่าหรือเกือบดำทั้งตัว และรูปร่างเพรียวกว่า มีลายสีเทาจางจัดขนาดใหญ่กว่าตัดกับผิวสีดำ มักรวมฝูงกลุ่มใหญ่ มีความคล่องตัวและว่ายได้โดยน้ำแทบไม่กระเพื่อมเมื่อต้องหลบหลีกภัย',
                image: "assets/img/9.jpg"
            }
        ];
    }
    WhalePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-whale',template:/*ion-inline-start:"E:\2561-2\Multimedia\Multimedia-59362505\Juju\multimedia-59362505\src\pages\whale\whale.html"*/'<!--\n  Generated template for the WhalePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n<ion-content padding class="bg  font">\n\n<ion-header>\n  <ion-navbar color="whaleinthai">\n    <ion-title>Whale In Thailand</ion-title>\n  </ion-navbar>\n</ion-header>\n\n  <ion-slides pager="true" effect="flip" >\n  <ion-slide *ngFor="let slide of slider">\n    <img [src]="slide.image" />\n    <h2 [innerHTML]="slide.title"></h2>\n    <h2 [innerHTML]="slide.title2"></h2>\n    <h4 [innerHTML]="slide.description"></h4>\n  </ion-slide>\n  </ion-slides>\n\n</ion-content>\n\n\n'/*ion-inline-end:"E:\2561-2\Multimedia\Multimedia-59362505\Juju\multimedia-59362505\src\pages\whale\whale.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], WhalePage);
    return WhalePage;
}());

//# sourceMappingURL=whale.js.map

/***/ }),

/***/ 125:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 125;

/***/ }),

/***/ 166:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/assignment/assignment.module": [
		329,
		2
	],
	"../pages/menu/menu.module": [
		330,
		1
	],
	"../pages/whale/whale.module": [
		331,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 166;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 169:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GoogleCloudVisionServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environment__ = __webpack_require__(171);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GoogleCloudVisionServiceProvider = /** @class */ (function () {
    function GoogleCloudVisionServiceProvider(http) {
        this.http = http;
    }
    GoogleCloudVisionServiceProvider.prototype.getLabels = function (base64Image) {
        var body = {
            "requests": [
                {
                    "image": {
                        "content": base64Image
                    },
                    "features": [
                        {
                            "type": "TEXT_DETECTION"
                        }
                    ]
                }
            ]
        };
        return this.http.post('https://vision.googleapis.com/v1/images:annotate?key=' + __WEBPACK_IMPORTED_MODULE_3__environment__["a" /* environment */].googleCloudVisionAPIKey, body);
    };
    GoogleCloudVisionServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], GoogleCloudVisionServiceProvider);
    return GoogleCloudVisionServiceProvider;
}());

//# sourceMappingURL=google-cloud-vision-service.js.map

/***/ }),

/***/ 171:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    firebaseConfig: {
        apiKey: "AIzaSyDYwvGwuThMirdcxzAkXEb_O3qxcfYh23w",
        authDomain: "newp-54b73.firebaseapp.com",
        databaseURL: "https://newp-54b73.firebaseio.com",
        projectId: "newp-54b73",
        storageBucket: "",
        messagingSenderId: "357125167409"
    },
    googleCloudVisionAPIKey: "AIzaSyDajiPujFd_T41PHfcnrFqPXA66WXTZvbQ"
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 225:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(246);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 246:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_about_about__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__ = __webpack_require__(325);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_google_cloud_vision_service_google_cloud_vision_service__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_camera__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_angularfire2__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_angularfire2_database__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_angularfire2_auth__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_menu_menu__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__environment__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__angular_http__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_whale_whale__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_assignment_assignment__ = __webpack_require__(117);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_whale_whale__["a" /* WhalePage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_assignment_assignment__["a" /* AssignmentPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_17__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/assignment/assignment.module#AssignmentPageModule', name: 'AssignmentPage', segment: 'assignment', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/menu/menu.module#MenuPageModule', name: 'MenuPage', segment: 'menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/whale/whale.module#WhalePageModule', name: 'WhalePage', segment: 'whale', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_12_angularfire2__["a" /* AngularFireModule */].initializeApp(__WEBPACK_IMPORTED_MODULE_16__environment__["a" /* environment */].firebaseConfig),
                __WEBPACK_IMPORTED_MODULE_13_angularfire2_database__["b" /* AngularFireDatabaseModule */],
                __WEBPACK_IMPORTED_MODULE_14_angularfire2_auth__["a" /* AngularFireAuthModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_whale_whale__["a" /* WhalePage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_assignment_assignment__["a" /* AssignmentPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_12_angularfire2__["a" /* AngularFireModule */],
                __WEBPACK_IMPORTED_MODULE_13_angularfire2_database__["b" /* AngularFireDatabaseModule */],
                __WEBPACK_IMPORTED_MODULE_14_angularfire2_auth__["a" /* AngularFireAuthModule */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_10__providers_google_cloud_vision_service_google_cloud_vision_service__["a" /* GoogleCloudVisionServiceProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_menu_menu__ = __webpack_require__(118);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_menu_menu__["a" /* MenuPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"E:\2561-2\Multimedia\Multimedia-59362505\Juju\multimedia-59362505\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"E:\2561-2\Multimedia\Multimedia-59362505\Juju\multimedia-59362505\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 325:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__about_about__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_contact__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(88);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__about_about__["a" /* AboutPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__contact_contact__["a" /* ContactPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"E:\2561-2\Multimedia\Multimedia-59362505\Juju\multimedia-59362505\src\pages\tabs\tabs.html"*/'<ion-tabs color="violet">\n \n  <ion-tab  [root]="tab1Root" tabTitle="Home" tabIcon="home" ></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="About" tabIcon="information-circle"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="Contact" tabIcon="contacts"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"E:\2561-2\Multimedia\Multimedia-59362505\Juju\multimedia-59362505\src\pages\tabs\tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_google_cloud_vision_service_google_cloud_vision_service__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(172);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ContactPage = /** @class */ (function () {
    function ContactPage(camera, vision, db, alert) {
        this.camera = camera;
        this.vision = vision;
        this.db = db;
        this.alert = alert;
        this.items = db.list('items');
    }
    ContactPage.prototype.takePhoto = function () {
        var _this = this;
        var options = {
            quality: 100,
            targetHeight: 500,
            targetWidth: 500,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.PNG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.vision.getLabels(imageData)
                .subscribe(function (result) {
                _this.saveResults(imageData, result.json().responses);
            }, function (err) {
                _this.showAlert(err);
            });
        }, function (err) {
            _this.showAlert(err);
        });
    };
    ContactPage.prototype.saveResults = function (imageData, results) {
        var _this = this;
        this.items.push({ imageData: imageData, results: results })
            .then(function (_) { })
            .catch(function (err) { _this.showAlert(err); });
    };
    ContactPage.prototype.showAlert = function (message) {
        var alert = this.alert.create({
            title: 'Error',
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    };
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"E:\2561-2\Multimedia\Multimedia-59362505\Juju\multimedia-59362505\src\pages\contact\contact.html"*/'<ion-header>\n    <ion-navbar color="whaleinthai">\n      <ion-title>API Google</ion-title>\n    </ion-navbar>\n  </ion-header>\n\n\n<ion-content padding class="bg font">\n    <ion-slides pager="true" effect="flip">\n        <ion-slide class="slide" *ngFor="let item of items | async"> \n          <img class="imgapi" [src]="\'data:image/png;base64,\' + item.imageData"/>\n    \n          <ion-list no-lines class="menu-home-close">\n          <ion-list-header style="font-size:16px">Labels</ion-list-header>\n            <label *ngFor="let label of item.results[0].textAnnotations">{{label.description}}</label>\n          </ion-list>\n        </ion-slide>\n      </ion-slides>\n\n  <ion-fab bottom right>\n  <button ion-fab (click)="takePhoto()">\n  <img class="topic1" src= "./assets/img/camera.svg" width="300px" height="250">\n  </button>\n  </ion-fab>\n\n </ion-content>'/*ion-inline-end:"E:\2561-2\Multimedia\Multimedia-59362505\Juju\multimedia-59362505\src\pages\contact\contact.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_3__providers_google_cloud_vision_service_google_cloud_vision_service__["a" /* GoogleCloudVisionServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 88:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"E:\2561-2\Multimedia\Multimedia-59362505\Juju\multimedia-59362505\src\pages\home\home.html"*/'<ion-header>\n  <ion-navbar color="whaleinthai">\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding class="background-home">\n  <span ion-fixed style="height: 100%; width: 100%;padding-left:15px ">\n  <!-- Header -->\n  <br>\n  <h1>Select Climate</h1>\n  <p></p>\n  <h1>Settings</h1>\n  <p></p>\n  <h5 style="color: #998ead">Favorite Presets</h5>\n  <!-- Slide -->\n  \n  <ion-slides slidesPerView="2.5" spaceBetween="10px" style="height: 30%">\n    <ion-slide>\n      <div class="menu-home-active">\n      <img src="../../assets/imgs/sun.svg" style="width:50%; height:50%">\n      <br>\n      <span>Heat</span>\n      <br>\n      <span style="font-size:150%;">10 ํ - 19 ํ</span>\n      </div>\n    </ion-slide>\n    \n    <ion-slide>\n      <div class="menu-home-close">\n      <img src="../../assets/imgs/snowflake.svg" style="width:50%; height:50%">\n      <br>\n      <span style="color:#6285c6">Cool</span>\n      <br>\n      <span style="font-size:150%;">3 ํ - 7 ํ</span>\n      </div>\n    </ion-slide>\n\n    <ion-slide>\n      <div class="menu-home-close">\n        <img src="../../assets/imgs/wind.svg" style="width:50%; height:50%">\n        <br>\n        <span style="color:#b381ff">Wet</span>\n        <br>\n        <span style="font-size:150%;">7 ํ - 10 ํ</span>\n        </div>\n    </ion-slide>\n\n    <ion-slide>\n      <div class="menu-home-close">\n        <img src="../../assets/imgs/dry2.svg" style="width:50%; height:50%">\n        <br>\n        <span style="color:#6285c6">Dry</span>\n        <br>\n        <span style="font-size:150%;">9 ํ - 14 ํ</span>\n        </div>\n    </ion-slide>\n    \n      \n</ion-slides>\n\n<div style="text-align: center;">\n        <br>\n        <br>\n        <span style="font-size:120px;">19</span>\n        <span style="font-size:28px;">temp</span>\n      </div>\n\n  \n  <!-- Wave Footer -->\n  <div class="wave"></div>\n  <div class="wave wave1"></div>\n  <div class="wave wave2"></div>\n  \n  </span>\n\n  </ion-content>\n\n  <ion-tabs color="violet">\n \n  \n  '/*ion-inline-end:"E:\2561-2\Multimedia\Multimedia-59362505\Juju\multimedia-59362505\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 89:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"E:\2561-2\Multimedia\Multimedia-59362505\Juju\multimedia-59362505\src\pages\about\about.html"*/'<ion-header>\n    <ion-navbar color="whaleinthai">\n      <ion-title>About</ion-title>\n    </ion-navbar>\n  </ion-header>\n\n<ion-content padding class="background-home ">\n  <div class="bg-pan-right"></div><br>\n  <h1>More Settings</h1>\n  <br>\n  \n  <div class="row" >\n    <div class="col col-2">\n      <span style="float: left;opacity: 0.5;">Modes</span>\n    </div>\n\n\n    <div class="col col-2">\n      <span style="float: right;opacity: 0.5;">Aromas</span>\n    </div>  \n\n\n  </div>\n  <div class="row">\n    <div class="col col-2">\n      <div class="model-circleheat"><img class="manu-top" \n        src="../../assets/imgs/sun.svg" style="float:center; "></div>\n      <br>\n      <div style="text-align: center; color: #ff3e62">Heat</div>\n    </div>\n\n\n    <div class="col col-2">\n      <div class="model-circle"><img class="manu-top" \n        src="../../assets/imgs/snowflake.svg" style="float:center; "></div>\n      <br>\n      <div style="text-align: center">Cool</div>\n    </div>\n    <div class="col col-2">\n  \n    </div>\n    <div class="col col-2">\n      <div class="model-circle"><img class="manu-top" \n        src="../../assets/imgs/cherry.svg" style="float:center; "></div>\n      <br>\n      <div style="text-align: center">Cherry</div>\n      \n    </div>\n    <div class="col col-2">\n      <div class="model-circleice"><img class="manu-top" \n        src="../../assets/imgs/ice.svg" style="float:center; "></div>\n      <br>\n      <div style="text-align: center;color: #00ccff">Ice</div>\n      \n    </div>\n  </div>\n  \n    <div class="row">\n      <div class="col col-2">\n        <div class="model-circle"><img class="manu-top" \n          src="../../assets/imgs/wind.svg" style="float:center; "></div>\n          <br>\n        <div style="text-align: center;">Wet</div>\n      </div>\n\n\n      <div class="col col-2">\n        <div class="model-circle"><img class="manu-top" \n          src="../../assets/imgs/dry2.svg" style="float:center; "></div>\n          <br>\n        <div style="text-align: center">Dry</div>\n      </div>\n\n\n      <div class="col col-2">\n      </div>\n      <div class="col col-2">\n        <div class="model-circle"><img class="manu-top" \n          src="../../assets/imgs/grape.svg" style="float:center; "></div>\n          <br>\n        <div style="text-align: center">Graph</div>\n        \n\n      </div>\n      <div class="col col-2">\n        <div class="model-circle"><img class="manu-top" \n          src="../../assets/imgs/lime.svg" style="float:center; "></div>\n          <br>\n        <div style="text-align: center">Lime</div>\n        \n      </div>\n\n      \n    </div>\n    <br><br><br>\n    <span style="float: left;opacity: 0.5;">Temperature</span>\n    <br>\n    <br>\n    <div style="text-align: center;">\n        <span style="font-size:120px;">19</span>\n        <span style="font-size:28px;">temp</span></div>\n  </ion-content>\n  '/*ion-inline-end:"E:\2561-2\Multimedia\Multimedia-59362505\Juju\multimedia-59362505\src\pages\about\about.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ })

},[225]);
//# sourceMappingURL=main.js.map